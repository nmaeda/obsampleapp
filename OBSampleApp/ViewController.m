//
//  ViewController.m
//  OBSampleApp
//
//  Created by nobukazu on 2019/06/08.
//  Copyright © 2019 nmaeda1218. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    // ノーマルの場合
    NSString *className = NSStringFromClass([UITableViewCell class]);
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:className];

    _tableItems = [self createTableItems];
}

- (NSArray *)createTableItems
{
    NSMutableArray *mArray = NSMutableArray.new;
    for (NSInteger i = 0; i < 10; i++) {
        [mArray addObject:[NSString stringWithFormat:@"%ld", (long)i]];
    }
    
    return [NSArray arrayWithArray:mArray];
}

#pragma mark - UITableViewDataSource
/**
 セクション数を返す
 @param tableView UITableViewインスタンス
 @return セクション数
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

/**
 セクションの要素数を返す
 @param tableView UITableViewインスタンス
 @param section セクション
 @return 要素数
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_tableItems count];
}

/**
 セルの高さを返す
 @param tableView UITableViewインスタンス
 @param indexPath NSIndexPathインスタンス
 @return せるの高さ
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

/**
 セルを返す
 @param tableView UITableViewインスタンス
 @param indexPath NSIndexPathインスタンス
 @return セルインスンタス
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *className = NSStringFromClass([UITableViewCell class]);
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:className
                                                            forIndexPath:indexPath];
    
    cell.textLabel.font = [UIFont systemFontOfSize:18];
    cell.textLabel.minimumScaleFactor = 9.f/18.f;
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    cell.textLabel.text = _tableItems[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
/**
 セルを選択すると呼ばれる
 @param tableView UITableViewインスタンス
 @param indexPath NSIndexPathインスタンス
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


@end
