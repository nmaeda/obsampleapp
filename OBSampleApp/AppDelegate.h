//
//  AppDelegate.h
//  OBSampleApp
//
//  Created by nobukazu on 2019/06/08.
//  Copyright © 2019 nmaeda1218. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

