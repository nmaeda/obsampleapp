//
//  main.m
//  OBSampleApp
//
//  Created by nobukazu on 2019/06/08.
//  Copyright © 2019 nmaeda1218. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
